<div class="ligfikstur bg-white shodoweffect2">
<div class="card">
	<div class="card-header row clear bg-white">
			<p class="link-title main-title col-8 text-left pl-4">PUAN<span class="normal"> DURUMU</span></p>
			<p  class="link-title col-4 text-right pr-4"><a href="#">TÜMÜ</a></p>
			<div class="card-header-left col-6"></div>
			<div class="card-header-right col-6"></div>
	</div>
<div class="card-body row clear">
	<ul class="nav nav-pills mt-2 mb-2" id="pills-tab7" role="tablist">
		<li class="nav-item col-3">
			<a class="nav-link active" id="pills-ligtab1-tab" data-toggle="pill" href="#pills-ligtab1" role="tab" aria-controls="pills-ligtab1" aria-selected="true"><img alt="daniga"  class="img-fluid full"  src="images/1.png"/></a>
		</li>
		<li class="nav-item col-3">
			<a class="nav-link" id="pills-ligtab2-tab" data-toggle="pill" href="#pills-ligtab2" role="tab" aria-controls="pills-ligtab2" aria-selected="false"><img alt="daniga"  class="img-fluid full"  src="images/2.png"/></a>
		</li>
		<li class="nav-item col-3">
			<a class="nav-link" id="pills-ligtab3-tab" data-toggle="pill" href="#pills-ligtab3" role="tab" aria-controls="pills-ligtab3" aria-selected="false"><img alt="daniga"  class="img-fluid full"  src="images/3.png"/></a>
		</li>
		<li class="nav-item col-3">
			<a class="nav-link" id="pills-ligtab4-tab" data-toggle="pill" href="#pills-ligtab4" role="tab" aria-controls="pills-ligtab4" aria-selected="false"><img alt="daniga"  class="img-fluid full"  src="images/4.png"/></a>
		</li>
	</ul>
	<div class="tab-content col-12 clear" id="pills-tab7Content">
		<div class="tab-pane fade show active" id="pills-ligtab1" role="tabpanel" aria-labelledby="pills-ligtab1-tab">

			  <table class="table table-striped table-hover clear">
				<thead>
				  <tr>
					<th>TAKIMLAR</th>
					<th>Av</th>
					<th>P</th>
				  </tr>
				</thead>
				<tbody class="table0">
				  <tr>
					<td>1.Galatasaray</td>
					<td>42</td>
					<td>75</td>
				  </tr>
				  <tr>
					<td>2.Fenerbahçe</td>
					<td>41</td>
					<td>74</td>
				  </tr>
				   <tr>
					<td>3.Beşiktaş</td>
					<td>40</td>
					<td>73</td>
				  </tr>
				  <tr>
					<td>4.Medipol Başakşehir</td>
					<td>39</td>
					<td>72</td>
				  </tr>
				  <tr>
					<td>5.Trabzonspor</td>
					<td>38</td>
					<td>71</td>
				  </tr>
				  <tr>
					<td>6.Göztepe</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tbody class="degisken gizlenen">
				   <tr>
					<td>7.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>8.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>9.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>10.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>11.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>12.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>13.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>14.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>15.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>16.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>17.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>18.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>19.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				   <tr>
					<td>20.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  </tbody>

				</tbody>

			  </table>
			  <button type="button"  onclick="ligfiksturhide(0)" class="btn col-12 bold  gizlenen btn-gizle"><i class="fas fa-caret-up text-success"></i> Daha Fazlasını Göster<br><span class="normal">(1-6)</span></button>
			  <button type="button"  onclick="ligfiksturshow(0)" class="btn col-12 bold  gosterilen btn-goster"><i class="fas fa-caret-down text-success"></i> Daha Fazlasını Göster<br><span class="normal">(1-20)</span></button>


		</div>
		<div class="tab-pane fade" id="pills-ligtab2" role="tabpanel" aria-labelledby="pills-ligtab2-tab">
			<table class="table table-striped table-hover clear">
			<thead>
				<tr>
				<th>TAKIMLAR</th>
				<th>Av</th>
				<th>P</th>
				</tr>
			</thead>
			<tbody class="table1">
				<tr>
				<td>1.Fenerbahçe</td>
				<td>42</td>
				<td>75</td>
				</tr>
				<tr>
				<td>2.Galatasaray</td>
				<td>41</td>
				<td>74</td>
				</tr>
				 <tr>
				<td>3.Beşiktaş</td>
				<td>40</td>
				<td>73</td>
				</tr>
				<tr>
				<td>4.Medipol Başakşehir</td>
				<td>39</td>
				<td>72</td>
				</tr>
				<tr>
				<td>5.Trabzonspor</td>
				<td>38</td>
				<td>71</td>
				</tr>
				<tr>
				<td>6.Göztepe</td>
				<td>37</td>
				<td>70</td>
				</tr>
				 <tbody class="degisken gizlenen">
				   <tr>
					<td>7.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>8.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>9.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>10.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>11.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>12.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>13.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>14.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>15.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>16.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>17.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>18.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>19.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				   <tr>
					<td>20.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  </tbody>
				

			</tbody>

			</table>
			<button type="button"  onclick="ligfiksturhide(1)" class="btn col-12 bold  gizlenen btn-gizle"><i class="fas fa-caret-up text-success"></i> Daha Fazlasını Göster<br><span class="normal">(1-6)</span></button>
			<button type="button"  onclick="ligfiksturshow(1)" class="btn col-12 bold  gosterilen btn-goster"><i class="fas fa-caret-down text-success"></i> Daha Fazlasını Göster<br><span class="normal">(1-20)</span></button>

		</div>
		<div class="tab-pane fade" id="pills-ligtab3" role="tabpanel" aria-labelledby="pills-ligtab3-tab">
			<table class="table table-striped table-hover clear">
			<thead>
				<tr>
				<th>TAKIMLAR</th>
				<th>Av</th>
				<th>P</th>
				</tr>
			</thead>
			<tbody class="table2">
				<tr>
				<td>1.Beşiktaş</td>
				<td>42</td>
				<td>75</td>
				</tr>
				<tr>
				<td>2.Fenerbahçe</td>
				<td>41</td>
				<td>74</td>
				</tr>
				 <tr>
				<td>3.Galatasaray</td>
				<td>40</td>
				<td>73</td>
				</tr>
				<tr>
				<td>4.Medipol Başakşehir</td>
				<td>39</td>
				<td>72</td>
				</tr>
				<tr>
				<td>5.Trabzonspor</td>
				<td>38</td>
				<td>71</td>
				</tr>
				<tr>
				<td>6.Göztepe</td>
				<td>37</td>
				<td>70</td>
				</tr>
				 <tbody class="degisken gizlenen">
				   <tr>
					<td>7.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>8.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>9.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>10.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>11.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>12.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>13.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>14.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>15.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>16.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>17.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>18.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>19.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				   <tr>
					<td>20.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  </tbody>
				

			</tbody>

			</table>
			<button type="button" onclick="ligfiksturhide(2)" class="btn col-12 bold  gizlenen btn-gizle"><i class="fas fa-caret-up text-success"></i> Daha Fazlasını Göster<br><span class="normal">(1-6)</span></button>
			<button type="button" onclick="ligfiksturshow(2)" class="btn col-12 bold  gosterilen btn-goster"><i class="fas fa-caret-down text-success"></i> Daha Fazlasını Göster<br><span class="normal">(1-20)</span></button>

		</div>
		<div class="tab-pane fade" id="pills-ligtab4" role="tabpanel" aria-labelledby="pills-ligtab4-tab">
			<table class="table table-striped table-hover clear">
			<thead>
				<tr>
				<th>TAKIMLAR</th>
				<th>Av</th>
				<th>P</th>
				</tr>
			</thead>
			<tbody class="table3">
				<tr>
				<td>1.Medipol Başakşehir</td>
				<td>42</td>
				<td>75</td>
				</tr>
				<tr>
				<td>2.Fenerbahçe</td>
				<td>41</td>
				<td>74</td>
				</tr>
				 <tr>
				<td>3.Beşiktaş</td>
				<td>40</td>
				<td>73</td>
				</tr>
				<tr>
				<td>4.Galatasaray</td>
				<td>39</td>
				<td>72</td>
				</tr>
				<tr>
				<td>5.Trabzonspor</td>
				<td>38</td>
				<td>71</td>
				</tr>
				<tr>
				<td>6.Göztepe</td>
				<td>37</td>
				<td>70</td>
				</tr>
				 <tbody class="degisken gizlenen">
				   <tr>
					<td>7.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>8.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>9.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>10.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>11.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>12.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>13.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>14.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>15.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>16.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>17.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>18.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  <tr>
					<td>19.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				   <tr>
					<td>20.xxxx</td>
					<td>37</td>
					<td>70</td>
				  </tr>
				  </tbody>
				

			</tbody>

			</table>
			<button type="button" onclick="ligfiksturhide(3)" class="btn col-12 bold  gizlenen btn-gizle"><i class="fas fa-caret-up text-success"></i> Daha Fazlasını Göster<br><span class="normal">(1-6)</span></button>
			<button type="button" onclick="ligfiksturshow(3)" class="btn col-12 bold  gosterilen btn-goster"><i class="fas fa-caret-down text-success"></i> Daha Fazlasını Göster<br><span class="normal">(1-20)</span></button>

		</div>

	 </div>
	</div>
  </div>
</div>
