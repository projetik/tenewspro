<div id="search"> 
	<span class="close">X</span>
	<form role="search" id="searchform" action="/search" method="get">
		<input value="" name="searchbox" type="search" placeholder="ARA"/>
	</form>
</div>

<div class="sticky-top fill-width-div">
<nav class="navbar  bg-white navbar-white  top-red-border bottom-gray-border">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand align-middle" href="#"><span><span id="logosize"><span id="logocolor">TE</span>NEWS</span>PRO</span></a>
	  <a class="navbar-brand navbar-items big-kategori align-middle bold" href="#"><i class="fas fa-images renk1"></i> GALERİLER</a>
	  <a class="navbar-brand navbar-itemsx big-kategori align-middle bold " href="#"><i class="fas fa-video renk1"></i> VİDEOLAR</a>
    </div>
	<div class="navbar-header">		
		<a href="#search" class='btn btn-arama btn-icon'><i class="fas fa-search"></i></a>
		<a href="#" class='btn btn-paylas btn-icon'><i class="far fa-bookmark"></i></a>
		<a href="#" class='btn btn-bildirim btn-icon'>
			<i class="far fa-bell"></i>
			<span id="notifications-count" class="text-danger">5</span>
		</a>
		<div class="dropdown">
		  <button class=" btn dropbtn btn-profil"><i class="fas fa-user"></i></button>
		  <div class="dropdown-content dropdown-menu-right">
			<a href="#">Giriş yap</a>
			<a href="#">Kayıt ol</a>
			<a href="#">Çıkış yap</a>
		  </div>
		</div>
	</div>
  </div>
</nav>




<nav class="navbar  fill-width-div navbar-expand bg-white font-weight-bold shodoweffect">
    <div class="container">
        <div class="navbar-collapse collapse">
            <ul class="navbar-nav bottom-navbar navbar-middle">
                <div class="navbar-brand dropdown">
                    <a class="nav-link pl-0" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-bars "></i>
					</a>
                    <div id="dd-m" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <?php include("dropdown-menu.php"); ?>                             
                    </div>
			</div>
				<a href="#" class="nav-link"><i class="fas fa-globe"></i> GÜNDEM</a>
				<a href="#" class="nav-link"><i class="fas fa-rocket"> </i> TEKNOLOJİ</a>
				<a href="#" class="nav-link"><i class="fas fa-futbol"> </i> SPOR</a>
				<a href="#" class="nav-link"><i class="fas fa-globe-africa"> </i> DÜNYA</a>
				<a href="#" class="nav-link"><i class="fas fa-comments"> </i> MAGAZİN</a>
				<a href="#" class="nav-link"><i class="fas fa-signature"> </i> EKONOMİ</a>
				<a href="#" class="nav-link"><i class="fab fa-pagelines"> </i> YAŞAM</a>
				<a href="#" class="nav-link"><i class="fas fa-car"> </i> OTOMOBİL</a>
				<a href="#" class="nav-link"><i class="fas fa-building"> </i> EMLAK</a>
            </ul>
        </div>
    </div>
</nav>

</div>
