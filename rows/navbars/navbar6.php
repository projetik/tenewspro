<div id="search"> 
	<span class="close">X</span>
	<form role="search" id="searchform" action="/search" method="get">
		<input value="" name="searchbox" type="search" placeholder="ARA"/>
	</form>
</div>

<div class="sticky-top fill-width-div">
<nav class="navbar  bg-white navbar-white  top-orange-border bottom-gray-border pt-3 pb-3">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand align-middle" href="#"><span><span id="logosize"><span id="logocolor">TE</span>NEWS</span>PRO</span></a>
    </div>
	<div class="navbar clear">
			<div class="box1">
				<span><span class="piyasa-title">EURO</span><br><span><span class="paradeger">7,41</span> <span class="paradegisim1">+0,01<i class="fas fa-caret-up"></i></span></span></span>
			</div>
			<div class="box1">
				<span><span class="piyasa-title">DOLAR</span><br><span><span class="paradeger">6,39</span> <span class="paradegisim2">-53,66<i class="fas fa-caret-down"></i></span></span></span>
			</div>
			<div class="box1">
				<span><span class="piyasa-title">ALTIN</span><br><span><span class="paradeger">1.196</span> <span class="paradegisim1">+0,1<i class="fas fa-caret-up"></i></span></span></span>
			</div>
			<div class="box1">
				<span><span class="piyasa-title">BİST</span><br><span><span class="paradeger">93.119</span> <span class="paradegisim2">-153,66<i class="fas fa-caret-down"></i></span></span></span>
			</div>
	</div>
	<div class="navbar">		
		<a href="#" class='btn btn-paylas btn-icon'><i class="far fa-bookmark"></i></a>
		<a href="#" class='btn btn-bildirim btn-icon'><i class="far fa-bell"></i> <span id="notifications-count" class="text-danger">5</span></a>
		<div class="dropdown">
		  <button class=" btn dropbtn btn-profil"><i class="fas fa-user"></i></button>
		  <div class="dropdown-content dropdown-menu-right">
			<a href="#">Giriş yap</a>
			<a href="#">Kayıt ol</a>
			<a href="#">Çıkış yap</a>
		  </div>
		</div>
	</div>
  </div>
</nav>




<nav class="navbar  fill-width-div navbar-expand bg-white font-weight-bold shodoweffect">
    <div class="container">
        <div class="navbar-collapse collapse">
            <ul class="navbar-nav bottom-navbar navbar-middle">
                <a href="#" class="nav-link"><i class="fas fa-globe"></i> GÜNDEM</a>
				<a href="#" class="nav-link"><i class="fas fa-rocket"> </i> TEKNOLOJİ</a>
				<a href="#" class="nav-link"><i class="fas fa-futbol"> </i> SPOR</a>
				<a href="#" class="nav-link"><i class="fas fa-globe-africa"> </i> DÜNYA</a>
				<a href="#" class="nav-link"><i class="fas fa-comments"> </i> MAGAZİN</a>
				<a href="#" class="nav-link"><i class="fas fa-signature"> </i> EKONOMİ</a>
				<a href="#" class="nav-link"><i class="fab fa-pagelines"> </i> YAŞAM</a>
				<a href="#" class="nav-link"><i class="fas fa-car"> </i> OTOMOBİL</a>
				<a href="#" class="nav-link"><i class="fas fa-flask"> </i> BİLM</a>
				 <div class="navbar-brand dropdown pl-2">
                    <a class="nav-link pl-0" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-bars "></i>
					</a>
                    <div id="dd-m" class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                          <?php include("dropdown-menu.php"); ?>         
                    </div>
			</div>
				<a href="#search" class='btn btn-arama'><i class="fas fa-search text-dark"></i></a>
            </ul>
        </div>
		
	</div>
</nav>

</div>
