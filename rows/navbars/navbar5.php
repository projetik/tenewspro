<div id="search"> 
	<span class="close">X</span>
	<form role="search" id="searchform" action="/search" method="get">
		<input value="" name="searchbox" type="search" placeholder="ARA"/>
	</form>
</div>

<nav class="navbar clear bg-white navbar-white fill-width-div  bottom-gray-border sticky-top">
  <div class="container">
    <div class="navbar">
      <p class="clear bold">
	  <?php 
	  echo iconv('latin5','utf-8',strftime(' %d %B %Y %A',strtotime(date('d F Y l'))));
	  ?>
</p>
    </div>
	<div class="navbar">
		<div class="box1">
			<span><span class="piyasa-title">EURO</span><br><span><span class="paradeger">7,41</span> <span class="paradegisim1">+0,01<i class="fas fa-caret-up"></i></span></span></span>
		</div>
		<div class="box1">
			<span><span class="piyasa-title">DOLAR</span><br><span><span class="paradeger">6,39</span> <span class="paradegisim2">-53,66<i class="fas fa-caret-down"></i></span></span></span>
		</div>
		<div class="box1">
			<span><span class="piyasa-title">ALTIN</span><br><span><span class="paradeger">1.196</span> <span class="paradegisim1">+0,1<i class="fas fa-caret-up"></i></span></span></span>
		</div>
	</div>
	<div class="navbar navbar-user-btn pl-1 pr-1">
		<a href="#" class='btn'><i class="fas fa-user"></i> ÜYE GİRİŞİ</a>
		<a href="#" class='btn brd-left'>ÜYE OLUN</a>
	</div>
	
  </div>
</nav>


<div class="sticky-top fill-width-div">

<nav class="navbar  bg-white navbar-white  bottom-gray-border sticky-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand align-middle" href="#"><span><span id="logosize"><span id="logocolor">TE</span>NEWS</span>PRO</span></a>
    </div>
	<div class="navbar clear">
	  <a class="navbar-brand navbar-itemsx big-kategori align-middle bold " href="#"><i class="fas fa-video renk1"></i> VİDEOLAR</a>
	  <a class="navbar-brand navbar-items big-kategori align-middle bold" href="#"><i class="fas fa-images renk1"></i> GALERİLER</a>
	  <a class="navbar-brand navbar-itemsx big-kategori align-middle bold " href="#"><i class="fas fa-feather renk1"></i> YAZARLAR</a>
	</div>
	<div class="nav-5-havadurumu">
		<span style="font-size:30px;float:left"><i style="color:#febe03" class="fas fa-sun"></i>12<sup></sup>°</span>
		<div>
			<select class="piyasa-title city-select">
				<option value="1">ANKARA</option>
				<option value="2">İSTANBUL</option>
				<option value="3">İZMİR</option>
			</select>
			<i class="fas fa-caret-down float-none"></i>
			<br><span style="color:#31708d" class="paradeger">Parçalı Bulutlu</span>
		</div>
	</div>
	
  </div>
</nav>
<nav class="navbar bottom-gray-border navbar-expand bg-white font-weight-bold">
    <div class="container">
        <div class="navbar-collapse collapse">
            <ul class="navbar-nav bottom-navbar navbar-middle">
                <a href="#" class='btn btn-arama'><i class="fas fa-home text-dark"></i></a> 
                <a href="#" class="nav-link"><i class="fas fa-globe"></i> GÜNDEM</a>
				<a href="#" class="nav-link"><i class="fas fa-rocket"> </i> TEKNOLOJİ</a>
				<a href="#" class="nav-link"><i class="fas fa-futbol"> </i> SPOR</a>
				<a href="#" class="nav-link"><i class="fas fa-globe-africa"> </i> DÜNYA</a>
				<a href="#" class="nav-link"><i class="fas fa-comments"> </i> MAGAZİN</a>
				<a href="#" class="nav-link"><i class="fas fa-signature"> </i> EKONOMİ</a>
				<a href="#" class="nav-link"><i class="fab fa-pagelines"> </i> YAŞAM</a>
				<a href="#" class="nav-link"><i class="fas fa-car"> </i> OTOMOBİL</a>
				 <div class="navbar-brand dropdown pl-2">
                    <a class="nav-link pl-0" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-bars "></i>
					</a>
                    <div  id="dd-m" class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <?php include("dropdown-menu.php"); ?>                      
                    </div>
			</div>
				<a href="#search" class='btn btn-arama'><i class="fas fa-search text-dark"></i></a>
            </ul>
        </div>
    </div>
</nav>
</div>

