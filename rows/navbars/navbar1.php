<div id="search"> 
	<span class="close">X</span>
	<form role="search" id="searchform" action="/search" method="get">
		<input value="" name="searchbox" type="search" placeholder="ARA"/>
	</form>
</div>

<nav class="navbar navbar1  bg-white navbar-white fill-width-div  top-red-border bottom-gray-border sticky-top">
  <div class="container">
    <div class="navbar-header">
				<div class="navbar-brand dropdown">
                    <a class="nav-link pl-0" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-bars "></i>
					</a>
                    <div id="dd-m" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                       <?php include("dropdown-menu.php"); ?>                      
                    </div>
			</div>
      <a class="navbar-brand align-middle" href="#"><span><span id="logosize"><span id="logocolor">TE</span>NEWS</span>PRO</span></a>

    </div>
	<div class="navbar navbar-middle">
		<a href="#" class="nav-link"><i class="fas fa-globe"></i> GÜNDEM</a>
		<a href="#" class="nav-link"><i class="fas fa-rocket"> </i> TEKNOLOJİ</a>
		<a href="#" class="nav-link"><i class="fas fa-futbol"> </i> SPOR</a>
		<a href="#" class="nav-link"><i class="fas fa-globe-africa"> </i> DÜNYA</a>
	
	</div>
	<div class="navbar">	
		<a href="#search" class='btn btn-arama btn-icon'><i class="fas fa-search"></i></a>
		<a href="#" class='btn btn-paylas btn-icon'><i class="far fa-bookmark"></i></a>
		<a href="#" class='btn btn-bildirim btn-icon'><i class="far fa-bell"></i><span id="notifications-count" class="text-danger">5</span></a>
		<div class="dropdown">
		  <button class=" btn dropbtn btn-profil"><i class="fas fa-user"></i></button>
		  <div class="dropdown-content dropdown-menu-right">
			<a href="#">Giriş yap</a>
			<a href="#">Kayıt ol</a>
			<a href="#">Çıkış yap</a>
		  </div>
		</div>
	</div>
  </div>
</nav>

