<div class="namazvakitleri shodoweffect2" >
<div class="card">
<div class="card-header row clear">
		<p class="link-title main-title col-7 text-left pl-3">NAMAZ VAKİTLERİ</p>
		<p  class="link-title col-5 text-right pr-3">14 TEM 2016</p>
</div>

	<div class="card-header row clear">
		<div class="col-12 center-div"><select class="custom-select">
			<option value="0" selected>İstanbul</option>
			<option value="1">Ankara</option>
			<option value="2">İzmir</option>
			<option value="3">Antalya</option>
		</select></div>
	</div>
	<div class="card-body row clear">
	<table class="table">
		<thead style="display:none">
			 <tr>
				<th></th>
				<th></th>
				<th></th>
			 </tr>
		</thead>
		<tbody>
			<tr>
				<td class="vakitad"><span class="vakitad-content">İMSAK</span></td>
				<td class="vakit"><span class="vakit-content">03:40</span></td>
				<td class="vakitaciklama"><p class="zamantukendi"><i class="far fa-check-circle renk5 col-3 clear"></i> <span class="col-9 clear">ALLAH KABUL ETSİN</span></p></td>
			</tr>
			<tr>
				<td class="vakitad"><span class="vakitad-content">GÜNEŞ</span></td>
				<td class="vakit"><span class="vakit-content">05:53</span></td>
				<td class="vakitaciklama"><p class="zamanakiyor"><b>3 Saat 05 Dakika 02 Saniye</b></p></td>
			</tr>
			<tr>
				<td class="vakitad"><span class="vakitad-content">ÖĞLE</span></td>
				<td class="vakit"><span class="vakit-content">13:10</span></td>
				<td class="vakitaciklama"><p></p></td>
			</tr>
			<tr>
				<td class="vakitad"><span class="vakitad-content">İKİNDİ</span></td>
				<td class="vakit"><span class="vakit-content">17:05</span></td>
				<td class="vakitaciklama"><p></p></td>
			</tr>
			<tr>
				<td class="vakitad"><span class="vakitad-content">AKŞAM</span></td>
				<td class="vakit"><span class="vakit-content">20:32</span></td>
				<td class="vakitaciklama"><p></p></td>
			</tr>
			<tr>
				<td class="vakitad"><span class="vakitad-content">YATSI</span></td>
				<td class="vakit"><span class="vakit-content">22:13</span></td>
				<td class="vakitaciklama"><p></p></td>
			</tr>
		</tbody>
	</table>
</div>
</div>
</div>