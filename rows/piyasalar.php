
<div class="piyasalar top-gray-border bottom-gray-border fill-width-div">
	<div class="container">
		<div class="row">
			<div class="box1">
				<span><span class="piyasa-title">PİYASALAR</span><br><span class="paradeger"  style="color:#a7aec2"><span>Güncelleme:</span> <span>02:46</span></span></span>
			</div>
			<div class="box1">
				<span><span class="piyasa-title">EURO</span><br><span><span class="paradeger">7,41</span> <span class="paradegisim1">+0,01<i class="fas fa-caret-up"></i></span></span></span>
			</div>
			<div class="box1">
				<span><span class="piyasa-title">DOLAR</span><br><span><span class="paradeger">6,39</span> <span class="paradegisim2">-53,66<i class="fas fa-caret-down"></i></span></span></span>
			</div>
			<div class="box1">
				<span><span class="piyasa-title">ALTIN</span><br><span><span class="paradeger">1.196</span> <span class="paradegisim1">+0,1<i class="fas fa-caret-up"></i></span></span></span>
			</div>
			<div class="box1">
				<span><span class="piyasa-title">BİST</span><br><span><span class="paradeger">93.119</span> <span class="paradegisim2">-153,66<i class="fas fa-caret-down"></i></span></span></span>
			</div>
			<div class="box2">
					<span style="font-size:30px;float:left"><i style="color:#febe03" class="fas fa-sun"></i>12<sup></sup>°</span>
					<div>
					<select class="piyasa-title city-select">
						<option value="1">ANKARA</option>
						<option value="2">İSTANBUL</option>
						<option value="3">İZMİR</option>
					</select>
					<i class="fas fa-caret-down float-none"></i>
					<br><span style="color:#31708d" class="paradeger">Parçalı Bulutlu</span></div>
			</div>
		</div>
	</div>
</div>

