
<div class="gunlukmansetler col-12" >
<div class="row clear" style="margin-bottom:10px !important">
<span class="baslik"><i class="fas fa-bookmark"></i> <b>GÜNLÜK</b> MANŞETLER</span>
<ul class="nav nav-pills "  style="font-size:14px"   id="pills-tab3" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-onecikanlar-tab" data-toggle="pill" href="#pills-onecikanlar" role="tab" aria-controls="pills-onecikanlar" aria-selected="true">ÖNE ÇIKANLAR</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-encokokunanlar-tab" data-toggle="pill" href="#pills-encokokunanlar" role="tab" aria-controls="pills-encokokunanlar" aria-selected="false">EN ÇOK OKUNANLAR</a>
  </li>
</ul>
</div>
<div class="tab-content" id="pills-tab3Content">
  <div class="tab-pane fade show active" id="pills-onecikanlar" role="tabpanel" aria-labelledby="pills-onecikanlar-tab">
	<div class="row">
	
	<div class="mansetbox col-4">
	 <div class="haberbox">
		<div class="resimKapsayici">
			<a href="#"><img alt="daniga"  src="images/gunluk-manset-1.jpg" class="img-fluid"/></a>
			<a href="#" class="btn resimYazisi resimYazisibutton bold" style="background-color:#fea928">TEKNOLOJİ</a>
		</div>
		<a href="#"><p class="g-m-p haber-module manset-line-clamp clear">Perhabs the most famous beach in Bermuda</p></a>
		<span class="read-time"><i class="far fa-clock"></i> 1 dakikada okunur</span>
	 </div>
	  <div class="haberbox mt-3">
		<div class="resimKapsayici">
			<a href="#"><img alt="daniga"  src="images/gunluk-manset-2.jpg" class="img-fluid"/></a>
			<a href="#" class="btn resimYazisi resimYazisibutton bold" style="background-color:#4bcbc9">TEKNOLOJİ</a>
		</div>
		<a href="#"><p class="g-m-p haber-module manset-line-clamp clear">Perhabs the most famous beach in Bermuda</p></a>
		<span class="read-time"><i class="far fa-clock"></i> 1 dakikada okunur</span>
	 </div>
	</div>
	<div class="mansetbox col-4" >
	  <div class="haberbox">
		<div class="resimKapsayici">
			<a href="#"><img alt="daniga"  src="images/gunluk-manset-3.jpg" class="img-fluid"/></a>
			<a href="#" class="btn resimYazisi resimYazisibutton bold" style="background-color:#35438e">MAGAZİN</a>
		</div>
		<a href="#"><p class="g-m-p haber-module manset-line-clamp clear">Perhabs the most famous beach in Bermuda</p></a>
		<span class="read-time"><i class="far fa-clock"></i> 1 dakikada okunur</span>
	  </div>
	  <div class="haberbox mt-3">
		<div class="resimKapsayici">
			<a href="#"><img alt="daniga"  src="images/gunluk-manset-4.jpg" class="img-fluid"/></a>
			<a href="#" class="btn resimYazisi resimYazisibutton bold" style="background-color:#2db759">SPOR</a>
		</div>
		<a href="#"><p class="g-m-p haber-module manset-line-clamp clear">Perhabs the most famous beach in Bermuda</p></a>
		<span class="read-time"><i class="far fa-clock"></i> 1 dakikada okunur</span>
	  </div>
	</div>
	<div class="mansetbox col-4">
	  <div class="haberbox">
		<div class="resimKapsayici bg-dark">
			<b  class="resimYazisi ozlu-soz" style="font-size:19px;line-height:30px">İtalyanlar size karşı galip gelemezler ama siz onlara mağlup olabilirsiniz.</b>
			<div class="cubuk"></div>
			<b class="resimYazisi1 ozlu-soz-yazar">Dean Rusk</b>
			<b class="resimYazisi2"><i class="fas fa-quote-left"></i></b>
		</div>
	  </div>
	  <div class="haberbox">
		<div  style="margin-top:45px;">
			<div class="row">
				<div class="minihaberbox  col-6">
					<a href="#"><img alt="daniga"  src="images/gunluk-manset-x1.jpg" class="img-fluid"/></a>
					<a href="#"><p class="haber-module manset-line-clamp clear">Hem estetik yaptırıp hem de doğal görünmenin Hem estetik yaptırıp hem de doğal görünmenin</p></a>
				</div>
				<div class="minihaberbox  col-6" >
					<a href="#"><img alt="daniga"  src="images/gunluk-manset-x2.jpg" class="img-fluid"/></a>
					<a href="#"><p class="haber-module manset-line-clamp clear">Hem estetik yaptırıp hem de doğal görünmenin</p></a>
				</div>
			</div>
			<div class="row mt-3">
				<div class="minihaberbox  col-6" >
					<a href="#"><img alt="daniga"  src="images/gunluk-manset-x3.jpg" class="img-fluid"/></a>
					<a href="#"><p class="haber-module manset-line-clamp clear">Hem estetik yaptırıp hem de doğal görünmenin</p></a>
				</div>
				<div class="minihaberbox  col-6" >
					<a href="#"><img alt="daniga"  src="images/gunluk-manset-x4.jpg" class="img-fluid"/></a>
					<a href="#"><p class="haber-module manset-line-clamp clear">Hem estetik yaptırıp hem de doğal görünmenin</p></a>
				</div>
			</div>
		</div>
	  </div>
	</div>
	</div>
  </div>

   <div class="tab-pane fade show" id="pills-encokokunanlar" role="tabpanel" aria-labelledby="pills-encokokunanlar-tab">
	<div class="row">
	
	<div class="mansetbox col-4">
	 <div class="haberbox">
		<div class="resimKapsayici">
			<a href="#"><img alt="daniga"  src="images/gunluk-manset-4.jpg" class="img-fluid"/></a>
			<a href="#" class="btn resimYazisi resimYazisibutton bold" style="background-color:#fea928">TEKNOLOJİ</a>		
		</div>
		<a href="#"><p class="g-m-p haber-module manset-line-clamp clear">Perhabs the most famous beach in Bermuda</p></a>
		<span class="read-time"><i class="far fa-clock"></i> 1 dakikada okunur</span>
	 </div>
	  <div class="haberbox mt-3">
		<div class="resimKapsayici">
			<a href="#"><img alt="daniga"  src="images/gunluk-manset-3.jpg" class="img-fluid"/></a>
			<a href="#" class="btn resimYazisi resimYazisibutton bold" style="background-color:#4bcbc9">TEKNOLOJİ</a>
		</div>
		<a href="#"><p class="g-m-p haber-module manset-line-clamp clear">Perhabs the most famous beach in Bermuda</p></a>
		<span class="read-time"><i class="far fa-clock"></i> 1 dakikada okunur</span>
	 </div>
	</div>
	<div class="mansetbox col-4" >
	  <div class="haberbox">
		<div class="resimKapsayici">
			<a href="#"><img alt="daniga"  src="images/gunluk-manset-2.jpg" class="img-fluid"/></a>
			<a href="#" class="btn resimYazisi resimYazisibutton bold" style="background-color:#35438e">MAGAZİN</a>
		</div>
		<a href="#"><p class="g-m-p haber-module manset-line-clamp clear">Perhabs the most famous beach in Bermuda</p></a>
		<span class="read-time"><i class="far fa-clock"></i> 1 dakikada okunur</span>
	  </div>
	  <div class="haberbox mt-3">
		<div class="resimKapsayici">
			<a href="#"><img alt="daniga"  src="images/gunluk-manset-1.jpg" class="img-fluid"/></a>
			<a href="#" class="btn resimYazisi resimYazisibutton bold" style="background-color:#2db759">SPOR</a>
		</div>
		<a href="#"><p class="g-m-p haber-module manset-line-clamp clear">Perhabs the most famous beach in Bermuda</p></a>
		<span class="read-time"><i class="far fa-clock"></i> 1 dakikada okunur</span>
	  </div>
	</div>
	<div class="mansetbox col-4">
	  <div class="haberbox">
		<div class="resimKapsayici bg-dark">
			<b  class="resimYazisi ozlu-soz" style="font-size:19px;line-height:30px">İtalyanlar size karşı galip gelemezler ama siz onlara mağlup olabilirsiniz.</b>
			<div class="cubuk"></div>
			<b class="resimYazisi1 ozlu-soz-yazar">Dean Rusk</b>
			<b class="resimYazisi2"><i class="fas fa-quote-left"></i></b>
		</div>
	  </div>
	  <div class="haberbox">
		<div  style="margin-top:45px;">
			<div class="row">
				<div class="minihaberbox  col-6" >
					<a href="#"><img alt="daniga"  src="images/gunluk-manset-x4.jpg" class="img-fluid"/></a>
					<a href="#"><p class="haber-module manset-line-clamp clear">Hem estetik yaptırıp hem de doğal görünmenin</p></a>
				</div> 
				<div class="minihaberbox  col-6">
					<a href="#"><img alt="daniga"  src="images/gunluk-manset-x3.jpg" class="img-fluid"/></a>
					<a href="#"><p class="haber-module manset-line-clamp clear">Hem estetik yaptırıp hem de doğal görünmenin</p></a>
				</div>
			</div>
			<div class="row mt-3">
				<div class="minihaberbox  col-6">
					<a href="#"><img alt="daniga"  src="images/gunluk-manset-x2.jpg" class="img-fluid"/></a>
					<a href="#"><p class="haber-module manset-line-clamp clear">Hem estetik yaptırıp hem de doğal görünmenin</p></a>
				</div>
				<div class="minihaberbox  col-6">
					<a href="#"><img alt="daniga"  src="images/gunluk-manset-x1.jpg" class="img-fluid"/></a>
					<a href="#"><p class="haber-module manset-line-clamp clear">Hem estetik yaptırıp hem de doğal görünmenin</p></a>
				</div>
			</div>
		</div>
	  </div>
	</div>
	</div>
  </div>
</div>

</div>




