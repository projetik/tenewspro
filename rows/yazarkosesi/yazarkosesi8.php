
<div class="yazarkosesi  yazarkosesi2 yazarkosesi3 yazarkosesi8 col-6 ">
  <ul class="nav nav-pills "  id="pills-tab2" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="pills-yazarlar-tab" data-toggle="pill" href="#pills-yazarlar" role="tab" aria-controls="pills-yazarlar" aria-selected="true"><i style="color:#ffa701" class="fas fa-feather"></i> YAZARLAR</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-alintiyazarlar-tab" data-toggle="pill" href="#pills-alintiyazarlar" role="tab" aria-controls="pills-alintiyazarlar" aria-selected="false">ALINTI YAZARLAR</a>
    </li>
  </ul>
  <div class="tab-content " id="pills-tab2Content">
    <div class="tab-pane fade show active" id="pills-yazarlar" role="tabpanel" aria-labelledby="pills-yazarlar-tab">
		<div class="row yazarrow shodoweffect2">
			<div class="yazarbox  col-11 not-brd-top not-brd-bottom">
				<div class="media">
					<a href="#"><img alt="daniga"  src="images/yazar2.jpg" class="align-self-center" ></a>
					<div class="media-body align-self-center">
						<a href="#"><h6>ÖMÜR GEDİK</h6></a>
						<a href="#"><h5 class="yazar-module yazar-line-clamp">Trump Neden Boşanmıyor! Trump Neden Boşanmıyor!</h5></a>
						<p>15 May 2018, 11:59</p>
					</div>
				</div>
							
			</div>
			<a href="#" class="btn align-bottom yazar-detay"><i class="fas fa-chevron-right"></i></a>		  
		</div>
		<div class="row yazarrow shodoweffect2">
			<div class="yazarbox  col-11 not-brd-top not-brd-bottom">
				<div class="media">
					<a href="#"><img alt="daniga"  src="images/yazar3.jpg" class="align-self-center" ></a>
					<div class="media-body align-self-center">
						<a href="#"><h6>YILMAZ ÖZDİL</h6></a>
						<a href="#"><h5 class="yazar-module yazar-line-clamp">Afrin dediğin sivilcedir, elma sirkesi birebirdir.</h5></a>
						<p>15 May 2018, 11:59</p>
					</div>
				</div>
							
			</div>
			<a href="#" class="btn align-bottom yazar-detay"><i class="fas fa-chevron-right"></i></a>		  
		</div>
		<div class="row yazarrow shodoweffect2">
			<div class="yazarbox  col-11 not-brd-top not-brd-bottom">
				<div class="media">
					<a href="#"><img alt="daniga"  src="images/yazar1.jpg" class="align-self-center" ></a>
					<div class="media-body align-self-center">
						<a href="#"><h6>HINCAL ULUÇ</h6></a>
						<a href="#"><h5 class="yazar-module yazar-line-clamp">Afrin dediğin sivilcedir, elma sirkesi birebirdir.</h5></a>
						<p>15 May 2018, 11:59</p>
					</div>
				</div>
							
			</div>
			<a href="#" class="btn align-bottom yazar-detay"><i class="fas fa-chevron-right"></i></a>		  
		</div>
		<div class="row yazarrow shodoweffect2">
			<div class="yazarbox  col-11 not-brd-top not-brd-bottom">
				<div class="media">
					<a href="#"><img alt="daniga"  src="images/murat-bardakci.jpg" class="align-self-center" ></a>
					<div class="media-body align-self-center">
						<a href="#"><h6>MURAT BARDAKÇI</h6></a>
						<a href="#"><h5 class="yazar-module yazar-line-clamp">Afrin dediğin sivilcedir, elma sirkesi birebirdir.</h5></a>
						<p>15 May 2018, 11:59</p>
					</div>
				</div>
							
			</div>
			<a href="#" class="btn align-bottom yazar-detay"><i class="fas fa-chevron-right"></i></a>		  
		</div>

     </div>
    <div class="tab-pane fades" id="pills-alintiyazarlar" role="tabpanel" aria-labelledby="pills-alintiyazarlar-tab">
             <div class="row yazarrow shodoweffect2">
			<div class="yazarbox  col-11 not-brd-top not-brd-bottom">
				<div class="media">
					<a href="#"><img alt="daniga"  src="images/yazar2.jpg" class="align-self-center" ></a>
					<div class="media-body align-self-center">
						<a href="#"><h6>ÖMÜR GEDİK</h6></a>
						<a href="#"><h5 class="yazar-module yazar-line-clamp">Trump Neden Boşanmıyor! Trump Neden Boşanmıyor!</h5></a>
						<p>15 May 2018, 11:59</p>
					</div>
				</div>
							
			</div>
			<a href="#" class="btn align-bottom yazar-detay"><i class="fas fa-chevron-right"></i></a>		  
		</div>
		<div class="row yazarrow shodoweffect2">
			<div class="yazarbox  col-11 not-brd-top not-brd-bottom">
				<div class="media">
					<a href="#"><img alt="daniga"  src="images/yazar3.jpg" class="align-self-center" ></a>
					<div class="media-body align-self-center">
						<a href="#"><h6>YILMAZ ÖZDİL</h6></a>
						<a href="#"><h5 class="yazar-module yazar-line-clamp">Afrin dediğin sivilcedir, elma sirkesi birebirdir.</h5></a>
						<p>15 May 2018, 11:59</p>
					</div>
				</div>
							
			</div>
			<a href="#" class="btn align-bottom yazar-detay"><i class="fas fa-chevron-right"></i></a>		  
		</div>
		<div class="row yazarrow shodoweffect2">
			<div class="yazarbox  col-11 not-brd-top not-brd-bottom">
				<div class="media">
					<a href="#"><img alt="daniga"  src="images/yazar1.jpg" class="align-self-center" ></a>
					<div class="media-body align-self-center">
						<a href="#"><h6>HINCAL ULUÇ</h6></a>
						<a href="#"><h5 class="yazar-module yazar-line-clamp">Afrin dediğin sivilcedir, elma sirkesi birebirdir.</h5></a>
						<p>15 May 2018, 11:59</p>
					</div>
				</div>
							
			</div>
			<a href="#" class="btn align-bottom yazar-detay"><i class="fas fa-chevron-right"></i></a>		  
		</div>
		<div class="row yazarrow shodoweffect2">
			<div class="yazarbox  col-11 not-brd-top not-brd-bottom">
				<div class="media">
					<a href="#"><img alt="daniga"  src="images/murat-bardakci.jpg" class="align-self-center" ></a>
					<div class="media-body align-self-center">
						<a href="#"><h6>MURAT BARDAKÇI</h6></a>
						<a href="#"><h5 class="yazar-module yazar-line-clamp">Afrin dediğin sivilcedir, elma sirkesi birebirdir.</h5></a>
						<p>15 May 2018, 11:59</p>
					</div>
				</div>
							
			</div>
			<a href="#" class="btn align-bottom yazar-detay"><i class="fas fa-chevron-right"></i></a>		  
		</div>
    </div>
   </div>
</div>
