<div class="dergiler shodoweffect2">
  <div class="card">
	  <div class="card-header row clear">
			<p class="link-title main-title col-8 text-left pl-3">DERGİLER</p>
			<p  class="link-title col-4 text-right pr-3"><a href="#">TÜMÜ</a></p>
			<div class="card-header-left col-6"></div>
			<div class="card-header-right col-6"></div>
		</div>
  <div class="card-body row clear">
	 <div class="swiper-container swiper-slider-26">
		<div class="swiper-wrapper">
		  <div class="swiper-slide">
			<div class="row col-12">
				<img alt="daniga"  class="img-fluid full" src="images/sanalkurs-1.jpg"/>
				<p class="col-12"><span class="text-dark bold">SanalKurs</span><br> <span>Haziran 2018</span></p>
			</div>
		  </div>
		  <div class="swiper-slide">
			<div class="row col-12">
				<img alt="daniga"  class="img-fluid full" src="images/sanalkurs-2.jpg"/>
				<p class="col-12"><span class="text-dark bold">SanalKurs</span><br> <span>Haziran 2018</span></p>
			</div>
		  </div>
		  <div class="swiper-slide">
			<div class="row col-12">
				<img alt="daniga"  class="img-fluid full" src="images/sanalkurs-3.jpg"/>
				<p class="col-12"><span class="text-dark bold">SanalKurs</span><br> <span>Haziran 2018</span></p>
			</div>
		  </div>
		</div>
		<div  class="swiper-pagination c-s-p"></div>
	  </div>

	 </div>
   </div>
</div>
