<div class="footer fill-width-div clear">

    <div class="container">
        <div class="row clear">
            <div class="col-4 logo">
                <h1 class="row">
                    <a class="align-middle text-white normalx pt-0" href="#"><span><span class="logosize bold"><span class="normalx">TE</span>NEWS</span>PRO</span></a>
                </h1>
               <h2 class="mt-1 normal">TE Bilişim, müşterileri için web’e yönelik çözümler üretir. Haber yazılımları, sunucu ve hosting hizmetleri, mobil uygulama, reklam çözümleri, CDN, player’lar ve teknik destek anlamında sonuca odaklı hizmetler sunar. Özellikle haber yazılımı konusunda Türkiye’de iyi imkânları sunan bir kuruluş olmanın sorumluluğunu sizlerle paylaşırken, en kaliteli çözümleri üretmek için de büyük gayret sarf ediyoruz.</h2>
            </div>
            <div class="col-5">
                <div class="row clear">
                    <div class="col-8 pl-2">
                        <h6 class="bold">KATEGORİLER</h6>
                        <div class="altcizgi"></div>
                        <div class="row clear">
                            <a href="#" class="col-6">Teknoloji</a>
                            <a href="#" class="col-6">Kadın</a>
                            <a href="#" class="col-6">Spor</a>
                            <a href="#" class="col-6">Politika</a>
                            <a href="#" class="col-6">Dünya</a>
                            <a href="#" class="col-6">Sağlık</a>
                            <a href="#" class="col-6">Magazin</a>
                            <a href="#" class="col-6">Seyahat</a>
                            <a href="#" class="col-6">Kültür-Sanat</a>
                            <a href="#" class="col-6">Ekonomi</a>
                            <a href="#" class="col-6">Teknoloji</a>
                            <a href="#" class="col-6">Yaşlam</a>
                        </div>

                    </div>
                    <div class="col-4">
                        <h6 class="bold">SAYFALAR</h6>
                        <div class="altcizgi"></div>
                        <div class="row clear">
                            <a href="#" class="col-12">Reklam</a>
                            <a href="#" class="col-12">Künye</a>
                            <a href="#" class="col-12">İhbar / Şikayet</a>
                            <a href="#" class="col-12">İletişim</a>
                            <a href="#" class="col-12">+Sitene Ekle</a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-3">
                <h6 class="bold">TAKİP EDİN</h6>
                <div class="altcizgi"></div>
                 <div class="text-left social-media">
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>   
                    <a href="#"><i class="fab fa-instagram"></i></a>                                 
                    <a href="#"><i class="fab fa-linkedin-in"></i></a>
                </div>
                <br>
                <div class="row clear">
                    <a href="#" class="col-12"><img alt="daniga"  class="img-fluid" src="images/gps.png"/></a>
                    <a href="#" class="col-12"><img alt="daniga"  class="img-fluid" src="images/aps.png"/></a>
                </div>


            </div>
        </div>
        <div class="footer-tags text-center mt-4">
            <button type="button" class="btn btn-light btn-sm">#siyaset</button>
            <button type="button" class="btn btn-light btn-sm">#politika</button>
            <button type="button" class="btn btn-light btn-sm">#yaşam</button>
            <button type="button" class="btn btn-light btn-sm">#türkiye</button>
            <button type="button" class="btn btn-light btn-sm">#spor</button>
            <button type="button" class="btn btn-light btn-sm">#finans</button>
            <button type="button" class="btn btn-light btn-sm">#kastamonu</button>
            <button type="button" class="btn btn-light btn-sm">#sakarya</button>
            <button type="button" class="btn btn-light btn-sm">#kayseri</button>
            <button type="button" class="btn btn-light btn-sm">#amasya</button>
            <button type="button" class="btn btn-light btn-sm">#hayat</button>
            <button type="button" class="btn btn-light btn-sm">#erdoğan</button>
            <button type="button" class="btn btn-light btn-sm">#kadın</button>
            <button type="button" class="btn btn-light btn-sm">#baskanık</button>
            <button type="button" class="btn btn-light btn-sm">#atletizm</button>
            <button type="button" class="btn btn-light btn-sm">#muzik</button>
            <button type="button" class="btn btn-light btn-sm">#halkinnabzi</button>
            <button type="button" class="btn btn-light btn-sm">#yazılım</button>
            <button type="button" class="btn btn-light btn-sm">#karadeniz</button>
            <button type="button" class="btn btn-light btn-sm">#oyun</button>
        </div>

    </div>
	<div class="row clear footer-bottom">
			<span class="col-12 text-center"><small>Copyright © <?php echo date("Y"); ?> Her hakkı saklıdır. Yazılım: 
                <a href="https://www.tebilisim.com" target="_blank" title="haber sistemi, haber scripti, haber yazılımı, tebilişim">
                    <img alt="daniga"  src="images/tebilisim.png" alt="TE Bilişim" style="margin:0 2px;margin-top:-5px;" height="15">
                </a>
				</small>
            </span>
	</div>
   
</div>
