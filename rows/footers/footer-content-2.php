<div class="footer fill-width-div clear footer2">

    <div class="container">
		<div class="row clear">
					<div class="col-4 logo">
						<h1 class="row">
							<a class="align-middle text-white normalx pt-0" href="#"><span><span class="logosize bold"><span class="normalx">TE</span>NEWS</span>PRO</span></a>
						</h1>
						 <h2 class="mt-1 normal">TE Bilişim, müşterileri için web’e yönelik çözümler üretir. Haber yazılımları, sunucu ve hosting hizmetleri, mobil uygulama, reklam çözümleri, CDN, player’lar ve teknik destek anlamında sonuca odaklı hizmetler sunar.</h2>
						 <div class="altcizgi"></div>
						 <p><i class="fas fa-map-marker-alt">&emsp;</i>Merkez Mh. Göktürk Cd. Su Venue Residence B Blok K5 N11 Göktürk / Eyüpsultan / İstanbul</p>
						 <p><i class="fas fa-phone">&emsp;</i>+90 (212) 322 74 90</p>
						 <p><i class="far fa-envelope">&emsp;</i>satis@tebilisim.com</p>
						 <div class="social-media pt-1">
							<a href="#"><i class="fab fa-facebook-f"></i></a>
							<a href="#"><i class="fab fa-twitter"></i></a>   
							<a href="#"><i class="fab fa-instagram"></i></a>                                 
							<a href="#"><i class="fab fa-linkedin-in"></i></a>
							<a href="#"><i class="fab fa-youtube"></i></a>
						</div>		
					</div>
				  
					 
					 
					 <div class="col-4 pl-2">
						  <h6 class="bold">güncel haberler</h6>
						   <div class="altcizgi"></div>

							<div class="media row hbr-box">
								<a href="#"><img alt="daniga"  class="align-self-center mr-3 img-fluid kategori-img" src="images/dolar_3100_tl_seviyesini_asti_h208_c2492_003.jpg" ></a>
								<div class="media-body align-self-center">
									  <a href="#"><p class="">Dolar 6.40 TL seviyesini aştı!</p></a>
									  <span class="read-time"><i class="far fa-clock"></i><?php echo iconv('latin5','utf-8',strftime(' %d %B %Y %A',strtotime(date('d F Y l')))); ?></span>
								</div>
							</div>
							 <hr/>
							<div class="media row hbr-box">
								<a href="#"><img alt="daniga"  class="align-self-center mr-3 img-fluid kategori-img" src="images/dolar_3100_tl_seviyesini_asti_h208_c2492_003.jpg" ></a>
								<div class="media-body align-self-center">
									  <a href="#"><p class="">Dolar 6.40 TL seviyesini aştı!</p></a>
									  <span class="read-time"><i class="far fa-clock"></i><?php echo iconv('latin5','utf-8',strftime(' %d %B %Y %A',strtotime(date('d F Y l')))); ?></span>
								</div>
							</div>
							<hr/>	   
							<div class="media row hbr-box">
								<a href="#"><img alt="daniga"  class="align-self-center mr-3 img-fluid kategori-img" src="images/dolar_3100_tl_seviyesini_asti_h208_c2492_003.jpg" ></a>
								<div class="media-body align-self-center">
									  <a href="#"><p class="">Dolar 6.40 TL seviyesini aştı!</p></a>
									  <span class="read-time"><i class="far fa-clock"></i><?php echo iconv('latin5','utf-8',strftime(' %d %B %Y %A',strtotime(date('d F Y l')))); ?></span>
								</div>
							</div>

					 </div>
							   

				 
					<div class="col-4 pl-2">
						  <h6 class="bold">en çok okunan haberler</h6>
						   <div class="altcizgi"></div>
						   <div class="media row hbr-box">
								<a href="#"><img alt="daniga"  class="align-self-center mr-3 img-fluid kategori-img" src="images/dolar_3100_tl_seviyesini_asti_h208_c2492_003.jpg" ></a>
								<div class="media-body align-self-center">
									  <a href="#"><p class="">Dolar 6.40 TL seviyesini aştı!</p></a>
									  <span class="read-time"><i class="far fa-clock"></i><?php echo iconv('latin5','utf-8',strftime(' %d %B %Y %A',strtotime(date('d F Y l')))); ?></span>
								</div>
							</div>
							<hr/>	   
							<div class="media row hbr-box">
								<a href="#"><img alt="daniga"  class="align-self-center mr-3 img-fluid kategori-img" src="images/dolar_3100_tl_seviyesini_asti_h208_c2492_003.jpg" ></a>
								<div class="media-body align-self-center">
									  <a href="#"><p class="">Dolar 6.40 TL seviyesini aştı!</p></a>
									  <span class="read-time"><i class="far fa-clock"></i><?php echo iconv('latin5','utf-8',strftime(' %d %B %Y %A',strtotime(date('d F Y l')))); ?></span>
								</div>
							</div>
							<hr/>	   
							<div class="media row hbr-box">
								<a href="#"><img alt="daniga"  class="align-self-center mr-3 img-fluid kategori-img" src="images/dolar_3100_tl_seviyesini_asti_h208_c2492_003.jpg" ></a>
								<div class="media-body align-self-center">
									  <a href="#"><p class="">Dolar 6.40 TL seviyesini aştı!</p></a>
									  <span class="read-time"><i class="far fa-clock"></i><?php echo iconv('latin5','utf-8',strftime(' %d %B %Y %A',strtotime(date('d F Y l')))); ?></span>
								</div>
							</div>

					 </div>
		</div>
		<div class="footer-tags text-center mt-4">
            <button type="button" class="btn btn-light btn-sm">#siyaset</button>
            <button type="button" class="btn btn-light btn-sm">#politika</button>
            <button type="button" class="btn btn-light btn-sm">#yaşam</button>
            <button type="button" class="btn btn-light btn-sm">#türkiye</button>
            <button type="button" class="btn btn-light btn-sm">#spor</button>
            <button type="button" class="btn btn-light btn-sm">#finans</button>
            <button type="button" class="btn btn-light btn-sm">#kastamonu</button>
            <button type="button" class="btn btn-light btn-sm">#sakarya</button>
            <button type="button" class="btn btn-light btn-sm">#kayseri</button>
            <button type="button" class="btn btn-light btn-sm">#amasya</button>
            <button type="button" class="btn btn-light btn-sm">#hayat</button>
            <button type="button" class="btn btn-light btn-sm">#erdoğan</button>
            <button type="button" class="btn btn-light btn-sm">#kadın</button>
            <button type="button" class="btn btn-light btn-sm">#baskanık</button>
            <button type="button" class="btn btn-light btn-sm">#atletizm</button>
            <button type="button" class="btn btn-light btn-sm">#muzik</button>
            <button type="button" class="btn btn-light btn-sm">#halkinnabzi</button>
            <button type="button" class="btn btn-light btn-sm">#yazılım</button>
            <button type="button" class="btn btn-light btn-sm">#karadeniz</button>
            <button type="button" class="btn btn-light btn-sm">#oyun</button>
        </div>   
</div>
	<div class="row clear footer-bottom">
			<span class="col-12 text-center"><small>Copyright © <?php echo date("Y"); ?> Her hakkı saklıdır. Yazılım: 
                <a href="https://www.tebilisim.com" target="_blank" title="haber sistemi, haber scripti, haber yazılımı, tebilişim">
                    <img  src="images/tebilisim.png" alt="TE Bilişim" style="margin:0 2px;margin-top:-5px;" height="15">
                </a>
				</small>
            </span>
	</div>
   
</div>
