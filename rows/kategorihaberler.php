
<div class="kategorihaberler row ">
<div class="col-12">

			<ul class="nav nav-pills mb-2 nav-fill row" id="pills-tab4" role="tablist">
			
					<li class="nav-item">
						
						<a  class="nav-link active" id="pills-kategori1-tab" data-toggle="pill" href="#pills-kategori1" role="tab" aria-controls="pills-kategori1" aria-selected="true"><span>GÜNDEM</span></a>
					</li>
		
				
			
					<li class="nav-item">
						<a  class="nav-link" id="pills-kategori2-tab" data-toggle="pill" href="#pills-kategori2" role="tab" aria-controls="pills-kategori2" aria-selected="false"><span>TEKNOLOJİ</span></a>
					</li>
			
			
					<li class="nav-item">
						<a  class="nav-link" id="pills-kategori3-tab" data-toggle="pill" href="#pills-kategori3" role="tab" aria-controls="pills-kategori3" aria-selected="false"><span>SPOR</span></a>
					</li>
			
			
					<li class="nav-item">
						<a  class="nav-link" id="pills-kategori4-tab" data-toggle="pill" href="#pills-kategori4" role="tab" aria-controls="pills-kategori4" aria-selected="false"><span>DÜNYA</span></a>
					</li>
			
			
					<li class="nav-item">
						<a  class="nav-link" id="pills-kategori5-tab" data-toggle="pill" href="#pills-kategori5" role="tab" aria-controls="pills-kategori5" aria-selected="false"><span>MAGAZİN</span></a>
					</li>
			
			
					<li class="nav-item">
						<a class="nav-link" id="pills-kategori6-tab" data-toggle="pill" href="#pills-kategori6" role="tab" aria-controls="pills-kategori6" aria-selected="false"><span>YAŞAM</span></a>
					</li>
				
			</ul>
			<div class="tab-content" id="pills-tab4Content">
				<div class="tab-pane fade show active" id="pills-kategori1" role="tabpanel" aria-labelledby="pills-kategori1-tab">
					<?php include("rows/ucs/gundemhaberler.php");?>
				</div>
				<div class="tab-pane fade rclass active" id="pills-kategori2" role="tabpanel" aria-labelledby="pills-kategori2-tab">
					<?php include("rows/ucs/teknolojihaberler.php");?>
				</div>
				<div class="tab-pane fade rclass active" id="pills-kategori3" role="tabpanel" aria-labelledby="pills-kategori3-tab">
					<?php include("rows/ucs/sporhaberler.php");?>
				</div>
				<div class="tab-pane fade rclass active" id="pills-kategori4" role="tabpanel" aria-labelledby="pills-kategori4-tab">
					<?php include("rows/ucs/dunyahaberler.php");?>
				</div>
				<div class="tab-pane fade rclass active" id="pills-kategori5" role="tabpanel" aria-labelledby="pills-kategori5-tab">
					<?php include("rows/ucs/magazinhaberler.php");?>
				</div>
				<div class="tab-pane fade rclass active" id="pills-kategori6" role="tabpanel" aria-labelledby="pills-kategori6-tab">
					<?php include("rows/ucs/yasamhaberler.php");?>
				</div>
			</div>
</div>
</div>
