<div class="firmarehberi shodoweffect2 bg-white">
	<div class="card">
		<div class="card-header row clear bg-white">
			<p class="link-title main-title col-9 pl-4">FİRMA <span class="normal">REHBERİ</span></p>
			<p  class="link-title col-3 pr-4"><a href="#">TÜM FİRMALAR</a></p>
			<div class="card-header-left col-6"></div>
			<div class="card-header-right col-6"></div>
		</div>
		<div class="card-body row clear">
			<div class="swiper-container swiper-slider-23">
                      <div class="swiper-wrapper">
                        <div class="swiper-slide">
						 <div class="row swiperbox">
							<a href="#" class="col-12"><img alt="daniga"  class="img-fluid " src="images/firma-rehberi.jpg"/></a>
							<p class="bold mt5 "><a href="#">ASELSAN</a><br><a href="#"><span class="color2 normal">SAVUNMA SANAYİ</span></a><br><a href="#" class="btn btn-outline-secondary btn mt-2">İSTANBUL</a></p>
						  </div>
						</div>
						<div class="swiper-slide">
						 <div class="row swiperbox">
							<a href="#" class="col-12"><img alt="daniga"  class="img-fluid " src="images/firma-rehberi2.jpg"/></a>
							<p class="bold mt5 "><a href="#">ASELSAN</a><br><a href="#"><span class="color2 normal">SAVUNMA SANAYİ</span></a><br><a href="#" class="btn btn-outline-secondary btn mt-2">İSTANBUL</a></p>
						  </div>
						</div>
						<div class="swiper-slide">
						 <div class="row swiperbox">
							<a href="#" class="col-12"><img alt="daniga"  class="img-fluid " src="images/firma-rehberi3.jpg"/></a>
							<p class="bold mt5 "><a href="#">ASELSAN</a><br><a href="#"><span class="color2 normal">SAVUNMA SANAYİ</span></a><br><a href="#" class="btn btn-outline-secondary btn mt-2">İSTANBUL</a></p>
						  </div>
						</div>
                      </div>
					  

						<div  class="swiper-pagination c-s-p"></div>
 
                 </div>
		</div>
	</div>
</div>


