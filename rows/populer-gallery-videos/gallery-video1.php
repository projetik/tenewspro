<div class="populer-video-gallery populer-video-gallery-color populer-video-gallery fill-width-div ">
	<div class="container p-v-g-content-color p-v-g-content">
	
		<div class="row" >
			
			<div class="col-6 pr-4">
					

					<div class="swiper-container swiper-slider-28">
						<div class="swiper-wrapper">
						  <div class="swiper-slide"><a href="#" class="resim-efekt"></a><img alt="daniga"  class="img-fluid " src="images/gunluk-manset-1.jpg"/><a href="#"><h6>Bu bir galeri 1</h6></a><p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">14</span></p><a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a></div>
						  <div class="swiper-slide"><a href="#" class="resim-efekt"></a><img alt="daniga"  class="img-fluid " src="images/gunluk-manset-2.jpg"/><a href="#"><h6>Bu bir galeri 2</h6></a><p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">17</span></p><a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a></div>
						  <div class="swiper-slide"><a href="#" class="resim-efekt"></a><img alt="daniga"  class="img-fluid " src="images/gunluk-manset-3.jpg"/><a href="#"><h6>Bu bir galeri 3</h6></a><p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">21</span></p><a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a></div>
						  <div class="swiper-slide"><a href="#" class="resim-efekt"></a><img alt="daniga"  class="img-fluid " src="images/gunluk-manset-4.jpg"/><a href="#"><h6>Bu bir galeri 4</h6></a><p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">57</span></p><a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a></div>
						  <div class="swiper-slide"><a href="#" class="resim-efekt"></a><img alt="daniga"  class="img-fluid " src="images/gunluk-manset-1.jpg"/><a href="#"><h6>Bu bir galeri 5</h6></a><p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">24</span></p><a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a></div>
						</div>
						
						<div  class="swiper-pagination c-s-p"></div>

                        <div class="populer-geleri-yonlendirici row text-center mt-3">
						  <h5 class="populer-video-gallery-title"><i class="fas fa-images">&nbsp;</i> POPÜLER GALERİLER</h5>
                          <button type="button" class="swiper-geri btn btn-light btn-lg lbutton"><i class="fas fa-chevron-left"></i></button>
                          <button  type="button" class="swiper-ileri btn btn-light btn-lg rbutton"><i class="fas fa-chevron-right"></i></button>
                        </div>
					</div>
			</div>
			
			
			<div class="col-6 pl-4" >
			
				<div class="swiper-container swiper-slider-28 videoslider">
						<div class="swiper-wrapper">
						  <div class="swiper-slide"><a href="#" class="resim-efekt"></a><img alt="daniga"  class="img-fluid " src="images/gunluk-manset-3.jpg"/><a href="#"><h6>Bu bir galeri 1</h6></a><p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">14</span></p><a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a></div>
						  <div class="swiper-slide"><a href="#" class="resim-efekt"></a><img alt="daniga"  class="img-fluid " src="images/gunluk-manset-1.jpg"/><a href="#"><h6>Bu bir galeri 2</h6></a><p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">17</span></p><a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a></div>
						  <div class="swiper-slide"><a href="#" class="resim-efekt"></a><img alt="daniga"  class="img-fluid " src="images/gunluk-manset-2.jpg"/><a href="#"><h6>Bu bir galeri 3</h6></a><p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">21</span></p><a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a></div>
						  <div class="swiper-slide"><a href="#" class="resim-efekt"></a><img alt="daniga"  class="img-fluid " src="images/gunluk-manset-4.jpg"/><a href="#"><h6>Bu bir galeri 4</h6></a><p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">57</span></p><a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a></div>
						  <div class="swiper-slide"><a href="#" class="resim-efekt"></a><img alt="daniga"  class="img-fluid " src="images/gunluk-manset-2.jpg"/><a href="#"><h6>Bu bir galeri 5</h6></a><p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">24</span></p><a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a></div>
						</div>
						
						<div  class="swiper-pagination c-s-p"></div>

                        <div class="populer-geleri-yonlendirici row text-center mt-3">
						  <h5 class="populer-video-gallery-title"><i class="fas fa-video">&nbsp;</i> POPÜLER VİDEOLAR</h5>
                          <button type="button" class="swiper-geri btn btn-light btn-lg lbutton"><i class="fas fa-chevron-left"></i></button>
                          <button  type="button" class="swiper-ileri btn btn-light btn-lg rbutton"><i class="fas fa-chevron-right"></i></button>
                        </div>
					</div>
			</div>
			
		</div> 
		
	</div>
</div>
