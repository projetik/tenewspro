
<div class="p-v-a-g shodoweffect2">
	<ul class="nav nav-pills row" id="pills-tab8" role="tablist">
	  <li class="nav-item col-6">
		<a class="nav-link active" id="pills-populergallery-tab" data-toggle="pill" href="#pills-populergallery" role="tab" aria-controls="pills-populergallery" aria-selected="true"><h6><i class="fas fa-images text-danger">&nbsp;</i> POPÜLER GALERİLER</h6></a>
	  </li>
	  <li class="nav-item col-6">
		<a class="nav-link" id="pills-populervideos-tab" data-toggle="pill" href="#pills-populervideos" role="tab" aria-controls="pills-populervideos" aria-selected="false"><h6><i class="fas fa-video text-danger">&nbsp;</i> POPÜLER VİDEOLAR</h6></a>
	  </li>
	</ul>
	<div class="tab-content" id="pills-tab8Content">
	  <div class="tab-pane fade show active" id="pills-populergallery" role="tabpanel" aria-labelledby="pills-populergallery-tab">
		
				<div class="swiper-container swiper-slider-29">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="row">
									<div class="col-6">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-1.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">12345</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-4.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">1234</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-6.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">123</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="row">
									<div class="col-6">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-2.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">12345</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-6.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">1234</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-4.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">123</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="row">
									<div class="col-6">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-1.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">12345</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-4.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">1234</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-6.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">123</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="row">
									<div class="col-6">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-2.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">12345</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-6.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">1234</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-4.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">123</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="row">
									<div class="col-6">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-1.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">12345</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-4.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">1234</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-6.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">123</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-camera"></i></a>
									</div>
								</div>
							</div>
						</div>
						
						<div  class="swiper-pagination c-s-p"></div>
				</div>
		
	  </div>
	  
	  
	  
	  
	  <div class="tab-pane fade active rclass6" id="pills-populervideos" role="tabpanel" aria-labelledby="pills-populervideos-tab">
	  
	  
				<div class="swiper-container swiper-slider-29">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="row">
									<div class="col-6">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-1.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">12345</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-4.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">1234</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-6.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">123</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="row">
									<div class="col-6">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-2.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">12345</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-6.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">1234</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-4.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">123</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="row">
									<div class="col-6">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-1.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">12345</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-4.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">1234</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-6.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">123</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="row">
									<div class="col-6">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-2.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">12345</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-6.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">1234</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-4.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">123</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="row">
									<div class="col-6">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-1.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">12345</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-4.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">1234</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
									<div class="col-3">
										<a href="#" class="resim-efekt"></a>
										<img alt="daniga"  class="img-fluid" src="images/popular-gallery-6.jpg"/>
										<a href="#"><h6>Bu bir yazı</h6></a>
										<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">123</span></p>
										<a class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
									</div>
								</div>
							</div>
						</div>
						
						<div  class="swiper-pagination c-s-p"></div>
				</div>
	</div>
	
	</div>
</div>