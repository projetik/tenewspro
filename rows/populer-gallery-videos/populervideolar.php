<div class="popularvideolar pt-3 pb-3 fill-width-div">
	<div class="container">
			<ul class="nav nav-pills mb-2" id="pills-tab" role="tablist">
				<i class="fas fa-video renk3">&nbsp;</i>
				<li class="nav-item">
					<a class="nav-link active" href="#">POPÜLER VİDEOLAR</a>
				</li>
				<li class="nav-item" style="border-left:1px solid #8b9290">
					<a class="nav-link" href="#">TÜM VİDEO GALERİLER</a>
				</li>
			</ul>
			
					<div class="row">
						<div class="vp-box col-6">
							<div class="resim-efekt"></div>
							<img alt="daniga"  class="img-fluid full p-absolute" src="images/popular-video-1.jpg"/>
							<a  href="#"  class="p-absolute big-playbutton"><i class="fas fa-play"></i></a>
						</div>
						<div class="pl-4 col-6">
							<a href="#"><h5 class="bold">Büyüleyen Ses Abdulbasit Abdussamed Ses Analizi</h5></a>
							<p class="bold"><i class="fas fa-video renk4">&nbsp;&nbsp;</i>396 kez görüntülendi</p>
							<a href="#"><p>Lorem ipsum dolor sit amet, vim ex affert aliquando conclusionemque, vim eu diam aeque lobortis. Ad has idque dicunt, eam in dolore erroribus reprehendunt. Copiosae vivendum ne duo, vix case legere intellegebat ad. Lorem minimum at pro. Ut nam mundi rationibus, nec meis errem utinam te. Per errem vidisse eu.</p></a>
						</div>
					</div>
					
					<div class="row mt-4">
					
					<div class="col-3">
						<div class="vp-box2">
							<div class="resim-efekt"></div>
							<img alt="daniga"  class="img-fluid full p-absolute" src="images/obama_ya_turkiye_tepkisi_h214_61a1b_003.jpg"/>
							<a  href="#"  class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
							<p class="bold p-absolute">Çukur'da Komiser Emrah Özür Diliyor</p>
						</div>
					</div>
					<div class="col-3">
						<div class="vp-box2">
							<div class="resim-efekt"></div>
							<img alt="daniga"  class="img-fluid full p-absolute" src="images/turkiyesiz_olmayacagini_anladilar_h212_a1f92.jpg"/>
							<a  href="#"  class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
							<p class="bold p-absolute">Çukur'da Komiser Emrah Özür Diliyor</p>
						</div>
					</div>
					<div class="col-3">
						<div class="vp-box2">
							<div class="resim-efekt"></div>
							<img alt="daniga"  class="img-fluid full p-absolute" src="images/bu_ogretmeni_gorevden_alin_h215_6d467.jpg"/>
							<a  href="#"  class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
							<p class="bold p-absolute">Çukur'da Komiser Emrah Özür Diliyor</p>
						</div>
					</div>
					<div class="col-3">
						<div class="vp-box2">
							<div class="resim-efekt"></div>
							<img alt="daniga"  class="img-fluid full p-absolute" src="images/volkan_konak_a_silahli_saldiri_h225_7edd6.jpg"/>
							<a  href="#"  class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
							<p class="bold p-absolute">Çukur'da Komiser Emrah Özür Diliyor</p>
						</div>
					</div>
						
					</div>
			</div>
		
	</div>

