<div class="populer-video-gallery2 shodoweffect2 fill-width-div">
	<div class="container">
		<div class="row">
			<h5 class="col-9"><i class="fas fa-video text-danger">&nbsp;</i> POPÜLER VİDEOLAR</h5>
			<a href="#" class="col-3 text-right pt-1 pr-2"><h6>Tüm VİDEOLAR</h6></a>
		</div>
		
		<div class="row p-v-g2-content">
			
			<div class="col-9">
				<div class="row  v-g-box full">
					<a href="#" class="resim-efekt"></a>
					<img alt="daniga"  class="img-fluid full" src="images/popular-video-1.jpg"/>
					<a  href="#"  class="p-absolute big-playbutton"><i class="fas fa-play"></i></a>
					<a href="#" class="v-g-box-content"><h6>Çukur'da Komiser Emrah Özür Diliyor</h6></a>
					<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">24</span></p>
				</div>
			</div>
			<div class="col-3">
				<div class="row v-g-box v-g-box2 ">
					<a href="#" class="resim-efekt"></a>
					<img alt="daniga"  class="img-fluid full" src="images/kr3.jpg"/>
					<a  href="#"  class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
					<a href="#" class="v-g-box-content"><h6>Çukur'da Komiser Emrah Özür Diliyor</h6></a>
					<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">24</span></p>
				</div>
				<div class="mr10"></div>
				<div class="row v-g-box v-g-box2 ">
					<a href="#" class="resim-efekt"></a>
					<img alt="daniga"  class="img-fluid full" src="images/kr2.jpg"/>
					<a  href="#"  class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
					<a href="#" class="v-g-box-content"><h6>Çukur'da Komiser Emrah Özür Diliyor</h6></a>
					<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">24</span></p>
				</div>
				<div class="mr10"></div>
				<div class="row v-g-box v-g-box2">
					<a href="#" class="resim-efekt"></a>
					<img alt="daniga"  class="img-fluid full" src="images/kr1.jpg"/>
					<a  href="#"  class="p-absolute small-playbutton"><i class="fas fa-play"></i></a>
					<a href="#" class="v-g-box-content"><h6>Çukur'da Komiser Emrah Özür Diliyor</h6></a>
					<p><i class="fas fa-eye"></i> <span class="p-v-g-reyting">24</span></p>
				</div>
			</div>
			
			
		</div>
	</div>
</div>