<div class="roportajlar shodoweffect2 bg-white">
  <div class="card">
	<div class="card-header row clear">
		<p class="link-title main-title col-8 text-left pl-3">ROPORTAJLAR</p>
		<p  class="link-title col-4 text-right pr-3"><a href="#">TÜMÜ</a></p>
		<div class="card-header-left col-6"></div>
		<div class="card-header-right col-6"></div>
	</div>
	<div class="card-body row clear">
	<div class="swiper-container swiper-slider-25">
		<div class="swiper-wrapper">
			<div class="swiper-slide">
				<div class="row  rowx">
					<img alt="daniga"  class="img-fluid col-5 clear" src="images/abdullah-tekin.jpg"/>
					<div class="col-7 pl-3">
						<p class="renk6 bold">ABDULLAH TEKİN</p>
						<p class="bold">Engelleri aşan yazılımcı...</p>
					</div>
				</div>			
			</div>
			<div class="swiper-slide">
				<div class="row  rowx">
					<img alt="daniga"  class="img-fluid col-5 clear" src="images/yazar1.jpg"/>
					<div class="col-7 pl-3">
						<p class="renk6 bold">Ad ve Soyad</p>
						<p class="bold">Açıklama</p>
					</div>
				</div>			
			</div>
			<div class="swiper-slide">
				<div class="row  rowx">
					<img alt="daniga"  class="img-fluid col-5 clear" src="images/yazar2.jpg"/>
					<div class="col-7 pl-3">
						<p class="renk6 bold">Ad ve Soyad</p>
						<p class="bold">Açıklama</p>
					</div>
				</div>			
			</div>
			<div class="swiper-slide">
				<div class="row rowx">
					<img alt="daniga"  class="img-fluid col-5 clear" src="images/yazar3.jpg"/>
					<div class="col-7 pl-3">
						<p class="renk6 bold">Ad ve Soyad</p>
						<p class="bold">Açıklama </p>
					</div>
				</div>			
			</div>
		</div>
		
		
		<div class="row  clear roportaj-yonlendirici">
			<button type="button" class="btn shodoweffect2  right"><i class="fas fa-chevron-left"></i></button>
			<button type="button" class="btn shodoweffect2  left"><i class="fas fa-chevron-right"></i></button>
		</div>
	</div>


  </div>
  </div>
</div>
