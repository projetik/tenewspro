

<form id="theme-panel">
  <i class="fa fa-cog fa-fw"></i>
  <div class="card">
    <div class="card-header">Tema Seçenekleri</div>
  </div>
 <div class="card-body">
    <div class="form-group">
      <label for="">Header</label>
     <select name="header-type" class="form-control" onchange="location = this.value;">
       <?php for ($i=1; $i <= 6; $i++) { ?>
        <option value="?header-type=<?=$i; ?>" <?php if ($theme_options['header_type'] == $i): echo 'selected'; endif; ?>><?=$i; ?></option>
       <?php } ?>
     </select>
   </div>
 </div>
</form>
<style>
  form#theme-panel {
      background: #fff;
      box-shadow: 0 0 6px rgba(0, 0, 0, .1);
      padding: 20px;
      position: fixed;
      top: 50%;
      right: -253px;
      transition: all .3s ease-in-out;
      width: 250px;
      z-index: 99;
  }
  form#theme-panel.show {
    right: 0;
  }
  form#theme-panel .fa-cog {
    cursor: pointer;
    position: absolute;
    top: 0;
    left: -40px;
    background: #fff;
    box-shadow: 0 0 6px rgba(0, 0, 0, .1);
    width: 40px;
    height: 40px;
    font-size: 22px;
    text-align: center;
    line-height: 40px;
  }
</style>
<?php
		include("js/javascript.php");
?>
<script>
  $('form#theme-panel .fa-cog').click(function() {
    $('form#theme-panel').toggleClass('show');
  });
</script>
</body>

</html>
