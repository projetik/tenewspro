<!DOCTYPE html >
<html lang="tr">
<head>
  <meta charset="utf-8">
  <title>TE News PRO</title>
	<meta name="viewport" content="width=device-width, initial-scale=0.1, shrink-to-fit=no">
	  <link rel="stylesheet" href="css/bootstrap.min.css">
	  <link rel="stylesheet" href="css/swiper.min.css">
	  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" >
	  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" >
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	  <link rel="stylesheet" href="css/mystyle.css"> 
	  <link rel="stylesheet" href="rows/ucs/swiperconfig.css"> 
	  <link rel="stylesheet" href="css/svg-turkiye-haritasi.css"/>
  
	<meta http-equiv="refresh" content="600" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta property="og:title" content="TE NEWS PRO - LX" />
	<meta name="twitter:url" content="https://tenewspro.herokuapp.com/" />
	<meta name="twitter:domain" content="https://tenewspro.herokuapp.com/" />
	<meta name="twitter:site" content="tenewspro" />
	<meta name="twitter:title" content="TE NEWS PRO - LX" />
	<meta name="twitter:description" content="TE Bilişim yeni tema çalışması TE NEWS PRO" />
	<meta name="twitter:image:src" content="images/anamanset-1.jpg" />
	<meta property="og:image:type" content="image/jpg" />
	<meta property="og:image:width" content="706" />
	<meta property="og:image:height" content="431" />
	<meta name="dc.language" content="tr">
	<meta name="dc.source" content="https://tenewspro.herokuapp.com/">
	<meta name="dc.title" content="TE NEWS PRO - LX">
	<meta name="dc.keywords" content="HTML,CSS,XML,JavaScript,TE,TE NEWS,news,haber">
	<meta name="dc.description" content="TE Bilişim yeni tema çalışması TE NEWS PRO">

	<link rel="dns-prefetch" href="//tenewspro.herokuapp.com">
	<link rel="dns-prefetch" href="//www.google-analytics.com">
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link rel="dns-prefetch" href="//mc.yandex.ru">
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<link rel="dns-prefetch" href="//pagead2.googlesyndication.com">
	<link rel="dns-prefetch" href="//googleads.g.doubleclick.net">
	<link rel="dns-prefetch" href="//google.com">
	<link rel="dns-prefetch" href="//gstatic.com">
	<link rel="dns-prefetch" href="//connect.facebook.net">
	<link rel="dns-prefetch" href="//graph.facebook.com">
	<link rel="dns-prefetch" href="//linkedin.com">
	<link rel="dns-prefetch" href="//ap.pinterest.com">
	<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
	<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8 ie7"> <![endif]-->
	<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9 ie8"> <![endif]-->
	<!--[if IE 9]>         <html class="no-js lt-ie10 ie9"> <![endif]-->
	<!--[if gt IE 9]>      <html class="no-js gt-ie9"> <![endif]-->
	<!--[if !IE] <![IGNORE[--><!--[IGNORE[]]-->
	
	
	<meta name="description" content="TE Bilişim yeni tema çalışması TE NEWS PRO"/>
	<meta name="googlebot" content="noarchive" />
	<meta name="subject" content="your website's subject">
	<meta name="copyright"content="Te Bilişim">
	<meta name="language" content="tr">
	<meta name="robots" content="index,follow" />
	<meta name="revised" content="Salı, Eylül 25, 2018, 11:48" />
	<meta name="abstract" content="">
	<meta name="topic" content="">
	<meta name="summary" content="">
	<meta name="Classification" content="news">
	<meta name="author" content="TE BİLİŞİM, satis@tebilisim.com">
	<meta name="designer" content="">
	<meta name="copyright" content="">
	<meta name="reply-to" content="satis@tebilisim.com">
	<meta name="owner" content="">
	<meta name="url" content="https://tenewspro.herokuapp.com/">
	<meta name="identifier-URL" content="https://tenewspro.herokuapp.com/">
	<meta name="directory" content="submission">
	<meta name="category" content="">
	<meta name="coverage" content="Worldwide">
	<meta name="distribution" content="Global">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	
	
	<meta name="og:title" content="TE Bilişim yeni tema çalışması TE NEWS PRO"/>
	<meta name="og:type" content="news"/>
	<meta name="og:url" content="https://tenewspro.herokuapp.com/"/>
	<meta name="og:image" content="images/anamanset-1.jpg"/>
	<meta name="og:site_name" content="TE NEWS PRO"/>
	<meta name="og:description" content="TE Bilişim yeni tema çalışması TE NEWS PRO"/>

	<meta name="fb:page_id" content="43929265776" />

	<meta name="og:email" content="satis@tebilisim.com"/>
	<meta name="og:phone_number" content="+90 (212) 322 74 90"/>
	<meta name="og:fax_number" content="+90 (212) 322 74 90"/>

	<meta name="og:latitude" content="37.416343"/>
	<meta name="og:longitude" content="-122.153013"/>
	<meta name="og:street-address" content="Merkez Mh. Göktürk Cd. Su Venue Residence B Blok K5 N11 Göktürk / Eyüpsultan / İstanbul"/>
	<meta name="og:locality" content="Palo Alto"/>
	<meta name="og:region" content="CA"/>
	<meta name="og:postal-code" content="34700"/>
	<meta name="og:country-name" content="TURKEY"/>

	<meta property="og:type" content="game.achievement"/>
	<meta property="og:points" content="POINTS_FOR_ACHIEVEMENT"/>

	<meta property="og:video" content="http://example.com/awesome.swf" />
	<meta property="og:video:height" content="640" />
	<meta property="og:video:width" content="385" />
	<meta property="og:video:type" content="application/x-shockwave-flash" />
	<meta property="og:video" content="http://example.com/html5.mp4" />
	<meta property="og:video:type" content="video/mp4" />
	<meta property="og:video" content="http://example.com/fallback.vid" />
	<meta property="og:video:type" content="text/html" />
	
	<meta name="google-analytics" content="1-AHFKALJ"/>
	<meta name="disqus" content="abcdefg"/>
	<meta name="uservoice" content="asdfasdf"/>
	<meta name="mixpanel" content="asdfasdf"/>
	
	<meta name="microid" content="mailto+http:sha1:e6058ed7fca4a1921cq91d7f1f3b8736cd3cc1g7" />
	
	
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta content="yes" name="apple-touch-fullscreen" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width = 320, initial-scale = 2.3, user-scalable = no">
	
	<meta http-equiv="Page-Enter" content="RevealTrans(Duration=2.0,Transition=2)" />
	<meta http-equiv="Page-Exit" content="RevealTrans(Duration=3.0,Transition=12)" />
	<meta name="mssmarttagspreventparsing" content="true">
	<meta http-equiv="X-UA-Compatible" content="chrome=1">
	<meta name="msapplication-starturl" content="http://blog.reybango.com/about/"/>
	<meta name="msapplication-window" content="width=800;height=600"/>
	<meta name="msapplication-navbutton-color" content="red"/>
	<meta name="application-name" content="Rey Bango Front-end Developer"/>
	<meta name="msapplication-tooltip" content="Launch Rey Bango's Blog"/>
	<meta name="msapplication-task" content="name=About;action-uri=/about/;icon-uri=/images/about.ico" />
	<meta name="msapplication-task" content="name=The Big List;action-uri=/the-big-list-of-javascript-css-and-html-development-tools-libraries-projects-and-books/;icon-uri=/images/list_links.ico" />
	<meta name="msapplication-task" content="name=jQuery Posts;action-uri=/category/jquery/;icon-uri=/images/jquery.ico" />
	<meta name="msapplication-task" content="name=Start Developing;action-uri=/category/javascript/;icon-uri=/images/script.ico" />
	<!--<link rel="shortcut icon" href="/images/favicon.ico" />-->
  
  
	<meta name="tweetmeme-title" content="Retweet Button Explained" />
	<meta name="blogcatalog" />
	
	<meta name="csrf-param" content="authenticity_token"/>
	<meta name="csrf-token" content="/bZVwvomkAnwAI1Qd37lFeewvpOIiackk9121fFwWwc="/>
	
	
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name= "viewport" content = "width = 320, initial-scale = 2.3, user-scalable = no">
	<meta name= "viewport" content = "width = device-width">
	<meta name = "viewport" content = "initial-scale = 1.0">
	<meta name = "viewport" content = "initial-scale = 2.3, user-scalable = no">
	<link rel="apple-touch-icon" href="touch-icon-iphone.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="touch-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="touch-icon-iphone4.png" />
	<link rel="apple-touch-startup-image" href="/startup.png">

	<!--<link rel="apple-touch-icon" type="image/png" href="/apple-touch-icon.png" />-->
	
	
	
	<link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/martini" />
  
  
  
	<meta name="theme-color" content="#4188c9">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta http-equiv="Content-Language" content="tr" />
	
	
 
</head>

<body>
<?php
try {
   date_default_timezone_set('Europe/Istanbul'); 
	setlocale(LC_TIME, "turkish");
	setlocale(LC_ALL,'turkish');
} catch (Exception $e) {
}
?>
